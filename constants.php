<?php
define ("REQUEST_ACTION_CREATE_USER", "create_user");
define ("REQUEST_ACTION_VALIDATE_USER", "validate_user");
define ("REQUEST_ACTION_LOG_IN", "log_in");
define ("REQUEST_ACTION_LOG_OUT", "log_out");
define ("REQUEST_ACTION_CREATE_POST", "create_post");
define ("REQUEST_ACTION_REMOVE_POST", "remove_post");
define ("REQUEST_ACTION_TOGGLE_COMMENTS", "toggle_comments");
define ("REQUEST_ACTION_LIKE_POST", "like_post");
define ("REQUEST_ACTION_UNLIKE_POST", "unlike_post");
define ("REQUEST_ACTION_FOLLOW_USER", "follow_user");
define ("REQUEST_ACTION_UNFOLLOW_USER", "unfollow_user");
define ("REQUEST_ACTION_UPDATE_PROFILE", "update_profile");
define ("REQUEST_ACTION_POST_COMMENT", "post_comment");
define ("REQUEST_ACTION_REMOVE_COMMENT", "remove_comment");
define ("REQUEST_ACTION_FORGOT_PASSWORD", "forgot_password");
define ("REQUEST_ACTION_FORGOT_PASSWORD_CHECK", "forgot_password_check");
define ("REQUEST_ACTION_FORGOT_PASSWORD_UPDATE", "forgot_password_update");
define ("REQUEST_ACTION_DEACTIVATE_ACCOUNT", "deactivate_account");


define ("SUCCESS_RESPONSE_CREATE_USER", "User Successfully Created. A verification email has been sent to the email provided.");
define ("SUCCESS_RESPONSE_VALIDATE_USER", "User Successfully Validated.");
define ("SUCCESS_RESPONSE_LOG_IN", "Logged in Successfully.");
define ("SUCCESS_RESPONSE_LOG_OUT", "Logged out Successfully.");
define ("SUCCESS_RESPONSE_CREATE_POST", "Post Created Successfully.");
define ("SUCCESS_RESPONSE_REMOVE_POST", "Post Removed Successfully.");
define ("SUCCESS_RESPONSE_TOGGLE_COMMENTS", "Comments Disabled Successfully.");
define ("SUCCESS_RESPONSE_LIKE_POST", "Post Liked Successfully.");
define ("SUCCESS_RESPONSE_UNLIKE_POST", "Post Un-Liked Successfully.");
define ("SUCCESS_RESPONSE_FOLLOW_USER", "User Followed Successfully.");
define ("SUCCESS_RESPONSE_UNFOLLOW_USER", "User Un-Followed Successfully.");
define ("SUCCESS_RESPONSE_UPDATE_PROFILE", "Profile Successfully Updated.");
define ("SUCCESS_RESPONSE_POST_COMMENT", "Comment Posted Successfully.");
define ("SUCCESS_RESPONSE_REMOVE_COMMENT", "Comment Removed Successfully.");
define ("SUCCESS_RESPONSE_FORGOT_PASSWORD", "Password Code Sent Successfully.");
define ("SUCCESS_RESPONSE_FORGOT_PASSWORD_CHECK", "Password Code Matches.");
define ("SUCCESS_RESPONSE_FORGOT_PASSWORD_UPDATE", "Password Successfully Updated.");
define ("SUCCESS_RESPONSE_DEACTIVATE_ACCOUNT", "Account Deactivated.");


define ("FAIL_RESPONSE_CREATE_USER", "Unable to Create User.");
define ("FAIL_RESPONSE_VALIDATE_USER", "Unable to Validate User.");
define ("FAIL_RESPONSE_LOG_IN", "Unable to Log in.");
define ("FAIL_RESPONSE_LOG_OUT", "Unable to Log out.");
define ("FAIL_RESPONSE_CREATE_POST", "Unable to Create Post.");
define ("FAIL_RESPONSE_REMOVE_POST", "Unable to Remove Post.");
define ("FAIL_RESPONSE_TOGGLE_COMMENTS", "Unable to Disable Comments.");
define ("FAIL_RESPONSE_LIKE_POST", "There was an issue liking this post.");
define ("FAIL_RESPONSE_UNLIKE_POST", "There was an issue un-liking this post.");
define ("FAIL_RESPONSE_FOLLOW_USER", "There was an issue following this user.");
define ("FAIL_RESPONSE_UNFOLLOW_USER", "There was an issue un-following this user.");
define ("FAIL_RESPONSE_UPDATE_PROFILE", "There was an issue updating your profile.");
define ("FAIL_RESPONSE_POST_COMMENT", "Unable to comment on this post.");
define ("FAIL_RESPONSE_REMOVE_COMMENT", "Unable to remove comment from this post.");
define ("FAIL_RESPONSE_FORGOT_PASSWORD", "There was an issue sending the update link.");
define ("FAIL_RESPONSE_FORGOT_PASSWORD_CHECK", "Password Reset Link no longer active.");
define ("FAIL_RESPONSE_FORGOT_PASSWORD_UPDATE", "There was an issue updating your password.");
define ("FAIL_RESPONSE_DEACTIVATE_ACCOUNT", "There was an issue Deactivating this account.");

define ("FAIL_RESPONSE_USER_NOT_LOGGED_IN", "You are not logged in.");
define ("FAIL_RESPONSE_ALREADY_LOGGED_IN", "This user is already logged in.");
define ("FAIL_RESPONSE_EMAIL_EXISTS", "There is already an account with that email address.");
define ("FAIL_RESPONSE_UNIQUE_NAME_EXISTS", "That user name is already in use.");
define ("FAIL_RESPONSE_EMAIL_NOT_EXISTS", "There is no user with that email address.");
define ("FAIL_RESPONSE_PASSWORDS_DO_NOT_MATCH", "The provided password does not match what is stored.");
define ("FAIL_RESPONSE_USER_NOT_ACTIVE", "This account is de-activated. To re-activate, please follow link in email.");
define ("FAIL_RESPONSE_NO_CONTENT_INCLUDED", "No content was included with the post.");
define ("FAIL_RESPONSE_INVALID_SESSION", "There is no active session with that id.");
define ("FAIL_RESPONSE_POST_ID_NOT_PROVIDED", "No Post ID was provided for this action.");
define ("FAIL_RESPONSE_POST_ID_DOES_NOT_BELONG_TO_ACTIVE_SESSION_USER", "The post you are trying to remove does not belong to this user.");
define ("FAIL_RESPONSE_USER_NAME_NOT_PROVIDED", "There was no included user name.");
define ("FAIL_RESPONSE_USER_NAME_NOT_EXIST", "The user does not exist.");
define ("FAIL_RESPONSE_CANNOT_FOLLOW_SELF", "You cannot follow yourself.");
define ("FAIL_RESPONSE_CANNOT_UNFOLLOW_SELF", "You cannot un-follow yourself.");
define ("FAIL_RESPONSE_NO_COMMENT_BODY", "There is no comment included to post.");
define ("FAIL_RESPONSE_COMMENTS_DISABLED", "Commenting has been disabled for this post.");
define ("FAIL_RESPONSE_COMMENT_NOT_USERS", "Cannot delete another user's comment.");
define ("FAIL_RESPONSE_COMMENT_ID_NOT_PROVIDED", "Comment ID not provided.");
define ("FAIL_RESPONSE_PASSWORD_RESET_CODE_NOT_PROVIDED", "Password reset code not provided.");
define ("FAIL_RESPONSE_PASSWORD_HASH_NOT_PROVIDED", "Updated password not provided.");
define ("FAIL_RESPONSE_PASSWORD_MATCHES_PROVIDED", "Cannot use current password.");
define ("FAIL_RESPONSE_EMAIL_NOT_PROVIDED", "No email has been provided.");

define ("REDIRECT_URL_LOGIN", "\login");

define ("EMAIL_TYPE_ACCOUNT_VERIFICATION", "account_verification");
define ("EMAIL_TYPE_PASSWORD_RESET", "password_reset");


define ("CACHE_GENERATOR_LIKE_POST", "like_post");
define ("CACHE_GENERATOR_UNLIKE_POST", "unlike_post");
define ("CACHE_GENERATOR_CREATE_POST", "create_post");
define ("CACHE_GENERATOR_REMOVE_POST", "remove_post");
define ("CACHE_GENERATOR_POST_COMMENT", "remove_post");
define ("CACHE_GENERATOR_REMOVE_COMMENT", "remove_post");
define ("CACHE_GENERATOR_FOLLOW_USER", "follow_user");
define ("CACHE_GENERATOR_UNFOLLOW_USER", "unfollow_user");
define ("CACHE_GENERATOR_CREATE_USER", "create_user");
define ("CACHE_GENERATOR_TOGGLE_COMMENTS", "toggle_comments");

?>