<?php
include 'database_read.php';
include 'database_write.php';
include 'classes.php';


////////READ\\\\\\\\\\\\\

function validateSession($session_id)
{
	//get user data using session id
	$ret = getUserBySession($session_id);
	if ($ret != null)
	{
		return $ret;
	}
	return false;
}
function getOrCreateSession($user_data, $device_info) 
{
	//get User_Session from User_Sessions matching user id
	$session = getSessionByUserID($user_data->id);
	if (isset($session))
	{
		return $session;
	}
	else
	{
		//create the session and return the value
		$session_id = generateHash($user_data->name . $user_data->unique_name . date("m.d.y h:i:sa"));
		return insertNewSession($user_data, date("m.d.y h:i:sa"), $session_id, $device_info);
	}
}
function doesEmailExist($address)
{
	$ret = getUserByEmail($address);
	if ($ret != null)
	{
		return $ret;
	}
	return false;
}
function doesUniqueNameExist($unique_name)
{
	$ret = getUserByUniqueName($unique_name);
	if ($ret != null)
	{
		return $ret;
	}
	return false;
}

//////////WRITE\\\\\\\\\\

//it's looking like these functions can be removed and the write actions can go direct to the database classes
function createUser($email, $unique_name, $name, $password_hash, $code)
{
	insertUser($email, $unique_name, $name, $password_hash, $code);
	return true;
}

function updateUser($user_data)
{
	return updateUserData($user_data);
}

function createPost($user_data, $post_data)
{
	return insertPost($user_data, $post_data);
}

function createCacheGeneratorInstruction($instruction, $param1, $param2=null, $param3=null)
{
	return insertInstruction($instruction, $param1, $param2, $param3);
}

//////////HELPERS\\\\\\\\\\\\

function generateHash($value)
{
	return md5($value);
}

function sendEmail($type, $email, $name, $code)
{
	//Hello, $name, here is your link. $code
	return true;
}

function uploadImage($user_data, $is_avatar)
{
	$avatarpath = ($is_avatar)?'/avatar/' : '/images';
	$filename = ($is_avatar)? 'avatar' : generateHash($user_data->id . date("m.d.y h:i:sa"));
	$uploaddir = '/var/'.$user_data->unique_name . $avatarpath;
	$uploadfile = $uploaddir . basename($_FILES['userfile']['name']) . $filename . $_FILES['userfile']['type'];

	if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
	    return $uploadfile;
	} else {
	    return null;
	}
}
?>