<?php

include 'config.php';

/////////////USER\\\\\\\\\\\\\\\\\
function getUserByEmail($address)
{
	global $database_url;
	global $database_name;
	global $database_username;
	global $database_password;
	$conn = new mysqli ($database_url, $database_username, $database_password, $database_name);
	if ($conn->connect_error) {
	  die("Connection failed: " . $conn->connect_error);
	}

	$stmt = $conn->prepare("SELECT * FROM Users WHERE email=? LIMIT 1");
	$stmt->bind_param("s", $address);
	$stmt->execute();
	$result = $stmt->get_result();
	$ret = new User ();
	if ($result->num_rows > 0) {
		//return just the first row
		$result_row = $result->fetch_assoc();

		$ret = createUserFromResponseData($result_row ?? null);
	} else {
		$ret = null;
	}
	$conn->close();
	return $ret;
}

function getUserBySession($session_id)
{
	global $database_url;
	global $database_name;
	global $database_username;
	global $database_password;
	$conn = new mysqli ($database_url, $database_username, $database_password, $database_name);
	if ($conn->connect_error) {
	  die("Connection failed: " . $conn->connect_error);
	}

	$stmt = $conn->prepare("SELECT user_id FROM user_sessions WHERE session_id=? LIMIT 1");
	$stmt->bind_param("s", $session_id);
	$stmt->execute();
	$result = $stmt->get_result();

	$ret = new User ();


	if ($result->num_rows > 0) {
		//return just the first row
		$result_row = $result->fetch_row();
		$ret = getUserByID($result_row ?? null);
	} else {
	  $ret = null;
	}
	$conn->close();
	return $ret;
}

function getUserByID($id)
{
	global $database_url;
	global $database_name;
	global $database_username;
	global $database_password;
	$conn = new mysqli ($database_url, $database_username, $database_password, $database_name);
	if ($conn->connect_error) {
	  die("Connection failed: " . $conn->connect_error);
	}

	$stmt = $conn->prepare("SELECT * FROM Users WHERE id=? LIMIT 1");
	$stmt->bind_param("s", $id);
	$stmt->execute();
	$result = $stmt->get_result();

	$ret = new User ();


	if ($result->num_rows > 0) {
		//return just the first row
		$result_row = $result->fetch_row();

		$ret = createUserFromResponseData($result_row ?? null);
	} else {
	  $ret = null;
	}
	$conn->close();
	return $ret;
}

function getUserByUniqueName($unique_name)
{
	global $database_url;
	global $database_name;
	global $database_username;
	global $database_password;
	$conn = new mysqli ($database_url, $database_username, $database_password, $database_name);
	if ($conn->connect_error) {
	  die("Connection failed: " . $conn->connect_error);
	}
	
	$stmt = $conn->prepare("SELECT * FROM Users WHERE unique_display_name=? LIMIT 1");
	$stmt->bind_param("s", $unique_name);
	$stmt->execute();
	$result = $stmt->get_result();

	$ret = new User ();

	if ($result->num_rows > 0) {
		//return just the first row
		$result_row = $result->fetch_assoc();

		$ret = createUserFromResponseData($result_row ?? null);
	} else {
		$ret = null;
	}
	$conn->close();
	return $ret;
}

function getSessionByUserID($user_id)
{
	//$val = SELECT * FROM User_Sessions WHERE User_Id=$user_id
	$ret = new Session();
	$ret->id = 3;
	$ret->date_started = date("m.d.y h:i:sa");
	$ret->session_id = "58b2951d8f80f82df82301364009139d";
	return ($user_id == 1)?$ret : null;
}

function getUserByPostId($post_id)
{
	global $database_url;
	global $database_name;
	global $database_username;
	global $database_password;
	$conn = new mysqli ($database_url, $database_username, $database_password, $database_name);
	if ($conn->connect_error) {
	  die("Connection failed: " . $conn->connect_error);
	}

	$stmt = $conn->prepare("SELECT user_id FROM posts WHERE post_id=? LIMIT 1");
	$stmt->bind_param("s", $post_id);
	$stmt->execute();
	$result = $stmt->get_result();

	$ret = new User ();

	if ($result->num_rows > 0) {
		//return just the first row
		$result_row = $result->fetch_row();
		$ret = getUserByID($result_row ?? null);
	} else {
	  $ret = null;
	}
	$conn->close();
	return $ret;
}

function doesUserExistByForgotPasswordCode($code)
{
	//val = query(SELECT 1 FROM Users WHERE Forgot_Password_Code=$code)
	//return val;
	return true;//if user exists
}

function getUserByForgotPasswordCode($code)
{
	//val = query(SELECT * FROM Users WHERE Forgot_Password_Code=$code)
	//return val;
	$ret = new User();
	$ret->id = 1;
	$ret->email = "taylor_754@hotmail.com";
	$ret->unique_name="tg754";
	$ret->name = "Taylor";
	$ret->is_active = true;
	$ret->password_hash="7cef6b14e9571b5795a56bcc1f36305e";
	return $ret;
}

////////GENERATE USER OBJECT\\\\\\\\\\\\\
function createUserFromResponseData($data)
{

	$ret = new User();
	if ($data != null)
	{
		$ret->id = $row["user_id"];
		$ret->email = $row["email"];
		$ret->name = $row["name"];
		$ret->unique_display_name = $row["unique_display_name"];
		$ret->mantra = $row["mantra"];
		$ret->avatar = $row["avatar"];
		$ret->total_posts = $row["total_posts"];
		$ret->password_hash = $row["password_hash"];
		$ret->verification_code = $row["verification_code"];
		$ret->forgot_password_code = $row["forgot_password_code"];
		$ret->first_login = $row["first_login"];
		$ret->is_validated = $row["is_validated"];
		$ret->is_active = $row["is_active"];
		$ret->join_date = $row["join_date"];
		$ret->email = $row["email"];
	} else {
		$ret = null;
	}
	return $ret;
}

///////////POSTS\\\\\\\\\\\\\\\\\\\\\\\

function getPostById($post_id)
{
	//$val = SELECT * FROM Posts WHERE Post_Id=$post_id
	//return val;
	$ret = new Post();
	$ret->id = 55;
	$ret->date = date("m.d.y h:i:sa");
	$ret->content = "This is a test post.";
	$ret->included_image = "";
	$ret->commenting_enabled = true;
	return $ret;
}

////////////COMMENTS\\\\\\\\\\\\\\

function getCommentById($comment_id)
{
	//$val = SELECT * FROM Comments WHERE Comment_Id=$comment_id
	//return val;
	$ret = new Comment();
	$ret->id = 4;
	$ret->date = date("m.d.y h:i:sa");
	$ret->content = "A Test Comment.";
	$ret->user_id = 1;
	$ret->deleted = false;
	return $ret;
}

///////FOLLOWING 
function findUserinFollowingList($user_id, $following_id)
{
	//val = SELECT * FROM User_Following WHERE User_Id=$user_id, Follow_Id=$following_id
	//if val.length > 0
	return true;
}


?>