<?php
include 'constants.php';
include 'utils.php';

if ($_SERVER['REQUEST_METHOD'] === "POST") {

	header("Content-Type: application/json");
	$data = "";
	if (!isset($_POST["action"]))
	{
		$data = json_decode(file_get_contents('php://input'));
		//not set up for json encdoded input
		if ($data != "" && $data->action)
		{
			echo json_encode("This server does not accomodate json encoded input at this time.");
		}
	}
	else //we're getting _POST form encoded data
	{
		$action = (isset($_POST["action"])) ? $_POST["action"] : "default";
		$output = new Output();
		switch ($action)
		{

			case REQUEST_ACTION_CREATE_USER :
				//email, name, password hash
				if (! (isset($_POST["email"]) && isset($_POST["name"]) && isset($_POST["unique_name"]) && isset($_POST["password_hash"]) ) )
				{
					$output->message = FAIL_RESPONSE_CREATE_USER;
				}
				else
				{
					//check if email is already used
					if (doesEmailExist($_POST["email"]))
					{
						//email is in use, exit
						$output->message = FAIL_RESPONSE_EMAIL_EXISTS;
					}
					else
					{
						//check if unique_name is taken
						if (doesUniqueNameExist($_POST["unique_name"]))
						{
							//user name is in use, exit
							$output->message = FAIL_RESPONSE_UNIQUE_NAME_EXISTS;
						}
						else
						{
							//generate a validation code
							$code = generateHash($_POST["name"]. $_POST["unique_name"] . date("m.d.y h:i:sa"));
							//create the entry in the database
							if (createUser($_POST["email"], $_POST["unique_name"], $_POST["name"], $_POST["password_hash"], $code))
							{
								//generate an email to send validation code
								sendEmail (EMAIL_TYPE_ACCOUNT_VERIFICATION, $_POST["email"], $_POST["name"], $code);
								$output->email = $_POST["email"];
								$output->code = $code;//only for testing purposes. Don't send them back the code for real!
								//UPDATE THE FEED GENERATOR
								createCacheGeneratorInstruction (CACHE_GENERATOR_CREATE_USER, $output->id);
								$output->message = SUCCESS_RESPONSE_CREATE_USER;
							}

						}
					}
					
					
				}

				echo json_encode($output);
				break;

			case REQUEST_ACTION_LOG_IN :
				//email, password hash, device_info
				if (! (isset($_POST["email"]) && isset($_POST["password_hash"]) ) )
				{
					$output->message = FAIL_RESPONSE_CREATE_USER;
				}
				else
				{
					$user_data = doesEmailExist($_POST["email"]);
					//check if a user exists with the email provided
					$output->user_data = $user_data;
					if ($user_data)
					{
						if ($user_data->is_active)//user has not been deactivated and does not need to be verified
						{
							if ($_POST["password_hash"] == $user_data->password_hash)
							{
								//user exists, passwords match
								//generate new session
								
								$device_info = isset($_POST["device_info"]) ? $_POST["device_info"] : "no info";
								$session_created = getOrCreateSession($user_data, $device_info); //just return existing session data if already logged in
								if ($session_created)
								{
									$output->session_id = $session_created->session_id;
									$output->session_created = $session_created;
									$output->message = SUCCESS_RESPONSE_LOG_IN;
								}
							}
							else
							{
								//passwords do not match, exit
								$output->message = FAIL_RESPONSE_PASSWORDS_DO_NOT_MATCH;
							}
						}
						else
							//user has been deactivated, send new code
						{
							//generate a validation code
							$code = generateHash($user_data->name . $user_data->unique_name . date("m.d.y h:i:sa"));
							//update user's verification code in the database
							$user_data->verification_code = $code;
							updateUser ($user_data);
							//generate an email to send validation code
							sendEmail ('re-validation',$_POST["email"], $user_data->name, $code);
							$output->email = $_POST["email"];
							$output->code = $code;//only for testing purposes. Don't send them back the code for real
							$output->message = SUCCESS_RESPONSE_LOG_IN;
						}
					}
					else
					{
						//email does not exist, exit
						$output->message = FAIL_RESPONSE_EMAIL_NOT_EXISTS;
					}
				}

				echo json_encode($output);
				break;

			case REQUEST_ACTION_LOG_OUT :
				//session id
				if (isset($_POST['session_id']))
				{
					$user_data = validateSession($_POST['session_id']);
					if ($user_data)
					{
						//user is currently logged in, log them out
						//will implement multi-session login/out later.
						removeSession($_POST['session_id']);
						$output->message = SUCCESS_RESPONSE_LOG_OUT;
					}
					else
					{
						$output->message = FAIL_RESPONSE_LOG_OUT;
					}
				}
				else 
				{
					//session id was not included
					$output->message = FAIL_RESPONSE_LOG_OUT;
				}
				echo json_encode($output);
				break;


			
			case REQUEST_ACTION_UPDATE_PROFILE :
				//session_id, name, avatar_data, password hash
				if (! isset($_POST["session_id"]) )
				{
					$output->message = FAIL_RESPONSE_CREATE_USER;
				}
				else
				{
					$user_data = validateSession($_POST["session_id"]);
					if ($user_data != null)
					{
						//if there's an image, upload it.
						if (isset($_FILES['userfile']['name']))
						{
							$path = uploadImage($user_data, true);
							$user_data->avatar = ($path != null) ? $path : $user_data->avatar;
						}
						
						//we have user data
						$user_data->name = isset($_POST["name"]) ? $_POST["name"] : $user_data->name;
						$user_data->mantra = isset($_POST["mantra"]) ? $_POST["mantra"] : $user_data->mantra;

						updateUser ($user_data);

						
						$output->message = SUCCESS_RESPONSE_UPDATE_PROFILE;
					}
				}

				echo json_encode ($output);
				break;

			case REQUEST_ACTION_CREATE_POST :
				//session_id, content, comments_enabled, image_data($_FILES)
				if (isset($_POST['session_id']))
				{
					$user_data = validateSession($_POST['session_id']);
					if ($user_data)
					{
						//user is currently logged in, we can post
						if ( isset($_POST['content']) )
						{
							$post = new Post();
							$post->content = $_POST['content'];
							$post->commenting_enabled = !isset($_POST['commenting_enabled']);
							if (isset($_FILES['userfile']['name']))
							{
								$path = uploadImage($user_data, true);
								$post->included_image = ($path != null) ? $path : $post->included_image;
							}
							$success = createPost($user_data, $post);
							$output->message = $success ? SUCCESS_RESPONSE_CREATE_POST : FAIL_RESPONSE_CREATE_POST;
							$output->data = $success; //post id or false
							//UPDATE THE FEED GENERATOR
							createCacheGeneratorInstruction (CACHE_GENERATOR_CREATE_POST, $output->data->id);

						}
						else 
						{
							//No data was included in the post
							$output->message = FAIL_RESPONSE_NO_CONTENT_INCLUDED;
						}
					}
					else
					{
						//session is not valid - TODO
						$output->message = FAIL_RESPONSE_CREATE_POST;
					}
				}
				else
				{
					//must include a session id
					$output->message = FAIL_RESPONSE_INVALID_SESSION;
				}
				echo json_encode($output);
				break;

			case REQUEST_ACTION_REMOVE_POST :
				//session_id, post_id
				if (isset($_POST['session_id']))
				{
					$user_data = validateSession($_POST['session_id']);
					if ($user_data)
					{
						if ( isset($_POST['post_id']) )
						{
							//make sure it belongs to the user
							$user_data_by_post = getUserByPostId($_POST['post_id']);
							if ($user_data_by_post->id == $user_data->id)
							{
								$success = removePostByPostId($_POST['post_id']);
								$output->message = $success ? SUCCESS_RESPONSE_REMOVE_POST : FAIL_RESPONSE_REMOVE_POST;
								//UPDATE THE FEED GENERATOR
								createCacheGeneratorInstruction (CACHE_GENERATOR_REMOVE_POST, $_POST['post_id']);
							}
							else
							{
								//can't remove someone else's post!
								$output->message = FAIL_RESPONSE_POST_ID_DOES_NOT_BELONG_TO_ACTIVE_SESSION_USER;
							}
						}
						else
						{
							//can't remove a post without knowing which one to remove!
							$output->message = FAIL_RESPONSE_POST_ID_NOT_PROVIDED;
						}
					}
					else
					{
						$output->message = FAIL_RESPONSE_INVALID_SESSION;
					}
				}
				else
				{
					//must include a session id
					$output->message = FAIL_RESPONSE_INVALID_SESSION;
				}
				echo json_encode($output);
				break;

			case REQUEST_ACTION_TOGGLE_COMMENTS :
				//session_id, post_id
				if (isset($_POST['session_id']))
				{
					$user_data = validateSession($_POST['session_id']);
					if ($user_data)
					{
						if ( isset($_POST['post_id']) )
						{
							//make sure it belongs to the user
							$user_data_by_post = getUserByPostId($_POST['post_id']);
							if ($user_data_by_post->id == $user_data->id)
							{
								$success = togglePostComments($_POST['post_id']);
								$output->message = $success ? SUCCESS_RESPONSE_TOGGLE_COMMENTS : FAIL_RESPONSE_TOGGLE_COMMENTS;
								//UPDATE THE FEED GENERATOR
								createCacheGeneratorInstruction (CACHE_GENERATOR_TOGGLE_COMMENTS, $_POST['post_id']);
							}
							else
							{
								//can't remove someone else's post!
								$output->message = FAIL_RESPONSE_POST_ID_DOES_NOT_BELONG_TO_ACTIVE_SESSION_USER;
							}
						}
						else
						{
							//can't toggle comments on a post without knowing which one it is!
							$output->message = FAIL_RESPONSE_POST_ID_NOT_PROVIDED;
						}
					}
					else
					{
						$output->message = FAIL_RESPONSE_INVALID_SESSION;
					}
				}
				else
				{
					//must include a session id
					$output->message = FAIL_RESPONSE_INVALID_SESSION;
				}
				echo json_encode($output);
				break;

			case REQUEST_ACTION_LIKE_POST :
				//session_id, post_id
				if (isset($_POST['session_id']))
				{
					$user_data = validateSession($_POST['session_id']);
					if ($user_data)
					{
						if ( isset($_POST['post_id']) )
						{
							$success = addLikeToPost($user_data->id, $_POST['post_id']);
							$output->message = $success ? SUCCESS_RESPONSE_LIKE_POST : FAIL_RESPONSE_LIKE_POST;
							//UPDATE THE FEED GENERATOR
							createCacheGeneratorInstruction (CACHE_GENERATOR_LIKE_POST, $_POST['post_id']);
						}
						else
						{
							//can't toggle comments on a post without knowing which one it is!
							$output->message = FAIL_RESPONSE_POST_ID_NOT_PROVIDED;
						}
					}
					else
					{
						//must include a session id
						$output->message = FAIL_RESPONSE_INVALID_SESSION;
					}
				}
				else
				{
					//must include a session id
					$output->message = FAIL_RESPONSE_INVALID_SESSION;
				}
				echo json_encode($output);
				break;

			case REQUEST_ACTION_UNLIKE_POST :
				//session_id, post_id
				if (isset($_POST['session_id']))
				{
					$user_data = validateSession($_POST['session_id']);
					if ($user_data)
					{
						if ( isset($_POST['post_id']) )
						{
							$success = removeLikeFromPost($user_data->id, $_POST['post_id']);
							$output->message = $success ? SUCCESS_RESPONSE_UNLIKE_POST : FAIL_RESPONSE_UNLIKE_POST;
							//UPDATE THE FEED GENERATOR
							createCacheGeneratorInstruction (CACHE_GENERATOR_UNLIKE_POST, $_POST['post_id']);
						}
						else
						{
							//can't toggle comments on a post without knowing which one it is!
							$output->message = FAIL_RESPONSE_POST_ID_NOT_PROVIDED;
						}
					}
					else
					{
						$output->message = FAIL_RESPONSE_INVALID_SESSION;
					}
				}
				else
				{
					//must include a session id
					$output->message = FAIL_RESPONSE_INVALID_SESSION;
				}
				echo json_encode($output);
				break;

			case REQUEST_ACTION_FOLLOW_USER :
				//session_id, user_to_follow_name
				if (isset($_POST['session_id']))
				{
					$user_data = validateSession($_POST['session_id']);
					if ($user_data)
					{
						if ( isset($_POST['user_to_follow_name']) )
						{
							//make sure it belongs to the user
							$follow_data = doesUniqueNameExist($_POST['user_to_follow_name']);
							if ($follow_data)
							{
								if ($follow_data->id != $user_data->id)
								{
									$success = followUser($user_data->id, $follow_data->id);
									$output->message = $success ? SUCCESS_RESPONSE_FOLLOW_USER : FAIL_RESPONSE_FOLLOW_USER;
									//UPDATE THE FEED GENERATOR
									createCacheGeneratorInstruction (CACHE_GENERATOR_FOLLOW_USER, $user_data->id, $_POST['user_to_follow_name']);
								}
								else
								{
									//can't follow yourself
									$output->message = FAIL_RESPONSE_CANNOT_FOLLOW_SELF;
								}
							}
							else
							{
								//can't follow a user that isn't specified
								$output->message = FAIL_RESPONSE_USER_NAME_NOT_EXIST;
							}
						}
						else
						{
							//can't follow a user that isn't specified
							$output->message = FAIL_RESPONSE_USER_NAME_NOT_PROVIDED;
						}
					}
					else
					{
						$output->message = FAIL_RESPONSE_INVALID_SESSION;
					}
				}
				else
				{
					//must include a session id
					$output->message = FAIL_RESPONSE_INVALID_SESSION;
				}
				echo json_encode($output);
				break;

			case REQUEST_ACTION_UNFOLLOW_USER :
				//session_id, user_to_follow_name
				if (isset($_POST['session_id']))
				{
					$user_data = validateSession($_POST['session_id']);
					if ($user_data)
					{
						if ( isset($_POST['user_to_unfollow_name']) )
						{
							//make sure it belongs to the user
							$follow_data = doesUniqueNameExist($_POST['user_to_unfollow_name']);
							if ($follow_data)
							{
								if ($follow_data->id != $user_data->id)
								{
									$success = unFollowUser($user_data->id, $follow_data->id);
									$output->message = $success ? SUCCESS_RESPONSE_UNFOLLOW_USER : FAIL_RESPONSE_UNFOLLOW_USER;
									//UPDATE THE FEED GENERATOR
									createCacheGeneratorInstruction (CACHE_GENERATOR_UNFOLLOW_USER, $user_data->id, $_POST['user_to_unfollow_name']);
								}
								else
								{
									//can't unfollow yourself
									$output->message = FAIL_RESPONSE_CANNOT_UNFOLLOW_SELF;
								}
							}
							else
							{
								//can't follow a user that isn't specified
								$output->message = FAIL_RESPONSE_USER_NAME_NOT_EXIST;
							}
						}
						else
						{
							//can't follow a user that isn't specified
							$output->message = FAIL_RESPONSE_USER_NAME_NOT_PROVIDED;
						}
					}
					else
					{
						$output->message = FAIL_RESPONSE_INVALID_SESSION;
					}
				}
				else
				{
					//must include a session id
					$output->message = FAIL_RESPONSE_INVALID_SESSION;
				}
				echo json_encode($output);
				break;

			case REQUEST_ACTION_POST_COMMENT :
				//session_id, post_id, comment_body
				if (isset($_POST['session_id']))
				{
					$user_data = validateSession($_POST['session_id']);
					if ($user_data)
					{
						if ( isset($_POST['post_id']) )
						{
							//get the post data so we know if commenting is enabled
							$post_data = getPostById($_POST['post_id']);
							if ($post_data && $post_data->commenting_enabled)
							{
								if (isset($_POST['comment_body']))
								{
									$success = addCommentToPost($user_data->id, $_POST['post_id'], $_POST['comment_body']);
									$output->message = $success ? SUCCESS_RESPONSE_POST_COMMENT : FAIL_RESPONSE_POST_COMMENT;
									//UPDATE THE FEED GENERATOR
									createCacheGeneratorInstruction (CACHE_GENERATOR_POST_COMMENT, $_POST['post_id']);
								}
								else
								{
									//can't post what we don't have
									$output->message = FAIL_RESPONSE_NO_COMMENT_BODY;
								}
							}
							else
							{
								//can't comment if it's disabled
								$output->message = FAIL_RESPONSE_COMMENTS_DISABLED;
							}
							
						}
						else
						{
							//can't comment on an unspecified post
							$output->message = FAIL_RESPONSE_POST_ID_NOT_PROVIDED;
						}
					}
					else
					{
						$output->message = FAIL_RESPONSE_INVALID_SESSION;
					}
				}
				else
				{
					//must include a session id
					$output->message = FAIL_RESPONSE_INVALID_SESSION;
				}
				echo json_encode($output);
				break;

			case REQUEST_ACTION_REMOVE_COMMENT :
				//session_id, comment_id
				if (isset($_POST['session_id']))
				{
					$user_data = validateSession($_POST['session_id']);
					if ($user_data)
					{
						if ( isset($_POST['comment_id']) )
						{
							//get the post data so we know if commenting is enabled
							$comment_data = getCommentById($_POST['comment_id']);
							if ($comment_data && ($comment_data->user_id == $user_data->id) )
							{
									$success = removeCommentById($comment_data->id);
									$output->message = $success ? SUCCESS_RESPONSE_REMOVE_COMMENT : FAIL_RESPONSE_REMOVE_COMMENT;
									//UPDATE THE FEED GENERATOR
									createCacheGeneratorInstruction (CACHE_GENERATOR_REMOVE_COMMENT, $comment_data->id);
							}
							else
							{
								//can't delete someone else's comment
								$output->message = FAIL_RESPONSE_COMMENT_NOT_USERS;
							}
							
						}
						else
						{
							//can't remove an unspecified comment
							$output->message = FAIL_RESPONSE_COMMENT_ID_NOT_PROVIDED;
						}
					}
					else
					{
						$output->message = FAIL_RESPONSE_INVALID_SESSION;
					}
				}
				else
				{
					//must include a session id
					$output->message = FAIL_RESPONSE_INVALID_SESSION;
				}
				echo json_encode($output);
				break;

			case REQUEST_ACTION_FORGOT_PASSWORD :
				//email
				if (isset($_POST['email']))
				{
					$user_data = getUserByEmail($_POST['email']);
					//generate a validation code
					$code = generateHash($user_data->name. $user_data->unique_name . date("m.d.y h:i:sa"));
					//create the entry in the database
					if ($user_data)
					{
						//generate an email to send validation code
						if (sendEmail (EMAIL_TYPE_PASSWORD_RESET, $_POST["email"], $user_data->name, $code))
						{
							$output->email = $_POST["email"];
							$output->code = $code;//only for testing purposes. Don't send them back the code for real!
							$output->message = SUCCESS_RESPONSE_FORGOT_PASSWORD;
						}
						else
						{
							$output->message = FAIL_RESPONSE_FORGOT_PASSWORD;
						}
					}
					else
					{
						$output->message = FAIL_RESPONSE_EMAIL_NOT_EXISTS;
					}
				}
				else
				{
					$output->message = FAIL_RESPONSE_EMAIL_NOT_PROVIDED;
				}
				echo json_encode($output);
				break;

			case REQUEST_ACTION_FORGOT_PASSWORD_CHECK :
				//forgot_password_code
				if (isset($_POST['forgot_password_code']))
				{
					//find user in database with this code set. If none found, reject the password update
					$success = doesUserExistByForgotPasswordCode($_POST['forgot_password_code']);
					$output->message = $success ? SUCCESS_RESPONSE_FORGOT_PASSWORD_CHECK : FAIL_RESPONSE_FORGOT_PASSWORD_CHECK;
				}
				else
				{
					$output->message = FAIL_RESPONSE_PASSWORD_RESET_CODE_NOT_PROVIDED;
				}
				echo json_encode($output);
				break;

			case REQUEST_ACTION_FORGOT_PASSWORD_UPDATE :
				//forgot_password_code, password_hash
				if (isset($_POST['forgot_password_code']))
				{
					if (isset($_POST['password_hash']))
					{
						//get user in database with this code set. 
						$user_data = getUserByForgotPasswordCode($_POST['forgot_password_code']);
						if ($_POST['password_hash'] === $user_data->password_hash)
						{
							$output->message = FAIL_RESPONSE_PASSWORD_MATCHES_PROVIDED;
						}
						else
						{
							$user_data->password_hash = $_POST['password_hash'];
							$success = updateUserData($user_data);
							$output->message = $success ? SUCCESS_RESPONSE_FORGOT_PASSWORD_UPDATE : FAIL_RESPONSE_FORGOT_PASSWORD_UPDATE;
						}
					}
					else
					{
						$output->message = FAIL_RESPONSE_PASSWORD_HASH_NOT_PROVIDED;
					}
				}
				else
				{
					$output->message = FAIL_RESPONSE_PASSWORD_RESET_CODE_NOT_PROVIDED;
				}
				echo json_encode($output);
				break;

			case REQUEST_ACTION_DEACTIVATE_ACCOUNT :
				echo json_encode(SUCCESS_RESPONSE_DEACTIVATE_ACCOUNT);
				break;

			case "default" :
				echo json_encode("Nothing to do here.");
				break;
		}
	}
}
else {
	echo json_encode("This server does not currently respond to GET requests.");
}
?>