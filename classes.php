<?php

class Output {};
class User {
	var $id;
	var $unique_name;
	var $name;
	var $avatar;
	var $mantra;
	var $post_count;
	var $is_active;
	var $password_hash;
	var $is_validated;
	var $email;
	var $verification_code;
	var $forgot_password_code;
}
class Session {
	var $id;
	var $date_started;
	var $session_id;
	var $device_info;
}

class Post {
	var $id;
	var $date;
	var $content;
	var $included_image;
	var $commenting_enabled = true;
}

class Comment {
	var $id;
	var $date;
	var $content;
	var $user_id;
	var $deleted;
}

?>