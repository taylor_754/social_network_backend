
DROP SCHEMA IF EXISTS `taylor_glazier_social_network`;

CREATE SCHEMA IF NOT EXISTS `taylor_glazier_social_network`;
USE `taylor_glazier_social_network`;



DROP TABLE IF EXISTS `taylor_glazier_social_network`.`Post_Likes`;
DROP TABLE IF EXISTS `taylor_glazier_social_network`.`Users_Following`;
DROP TABLE IF EXISTS `taylor_glazier_social_network`.`User_Sessions`;
DROP TABLE IF EXISTS `taylor_glazier_social_network`.`User_Notifications`;
DROP TABLE IF EXISTS `taylor_glazier_social_network`.`Comments`;
DROP TABLE IF EXISTS `taylor_glazier_social_network`.`Posts`;
DROP TABLE IF EXISTS `taylor_glazier_social_network`.`Users`;

CREATE TABLE `taylor_glazier_social_network`.`Users` (
    user_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    name VARCHAR(30) NOT NULL,
    unique_display_name VARCHAR(30) NOT NULL,
    mantra VARCHAR(65),
    email VARCHAR(50) NOT NULL,
    avatar VARCHAR(256),
    total_posts INT UNSIGNED,
    password_hash VARCHAR(128),
    verification_code VARCHAR (128),
    forgot_password_code VARCHAR(128),
    first_login TINYINT(1),
    is_validated TINYINT(1),
    is_active TINYINT(1),
    join_date DATE,
    PRIMARY KEY ( user_id )
);



CREATE TABLE `taylor_glazier_social_network`.`Users_Following`(
    user_id INT UNSIGNED NOT NULL,
    is_following_user_id INT UNSIGNED NOT NULL,
    FOREIGN KEY (user_id) REFERENCES Users (user_id) ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY (is_following_user_id) REFERENCES Users (user_id) ON DELETE RESTRICT ON UPDATE CASCADE,
    PRIMARY KEY (user_id, is_following_user_id)
);
    



CREATE TABLE `taylor_glazier_social_network`.`Posts`(
    post_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    user_id INT UNSIGNED NOT NULL,
    date_posted DATE NOT NULL,
    content VARCHAR(256),
    included_image VARCHAR(256),
    commenting_enabled TINYINT(1),
    FOREIGN KEY (user_id) REFERENCES Users(user_id) ON DELETE RESTRICT ON UPDATE CASCADE,
    PRIMARY KEY (post_id)
);



CREATE TABLE `taylor_glazier_social_network`.`Post_Likes`(
    post_id INT UNSIGNED NOT NULL,
    user_id INT UNSIGNED NOT NULL,
    FOREIGN KEY (post_id) REFERENCES Posts (post_id) ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY (user_id) REFERENCES Users (user_id) ON DELETE RESTRICT ON UPDATE CASCADE,
    PRIMARY KEY (post_id, user_id)
);



CREATE TABLE `taylor_glazier_social_network`.`Comments`(
    comment_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    post_id INT UNSIGNED NOT NULL,
    user_id INT UNSIGNED NOT NULL,
    date_posted DATE NOT NULL,
    comment_content VARCHAR(256),
    deleted TINYINT(1),
    FOREIGN KEY (user_id) REFERENCES Users(user_id) ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY (post_id) REFERENCES Posts(post_id) ON DELETE RESTRICT ON UPDATE CASCADE,
    PRIMARY KEY (comment_id)
);

DROP TABLE IF EXISTS `taylor_glazier_social_network`.`Sessions`;

CREATE TABLE `taylor_glazier_social_network`.`Sessions`(
    session_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    date_started DATE NOT NULL,
    session_key VARCHAR(128) NOT NULL,
    device_info VARCHAR(128),
    PRIMARY KEY ( session_id )
);



CREATE TABLE `taylor_glazier_social_network`.`User_Sessions`(
    user_id INT UNSIGNED NOT NULL,
    session_id INT UNSIGNED NOT NULL,
    FOREIGN KEY (user_id) REFERENCES Users (user_id) ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY (session_id) REFERENCES Sessions (session_id) ON DELETE RESTRICT ON UPDATE CASCADE,
    PRIMARY KEY (user_id, session_id)
);

DROP TABLE IF EXISTS `taylor_glazier_social_network`.`Cache_Instructions`;

CREATE TABLE `taylor_glazier_social_network`.`Cache_Instructions`(
    instruction_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    instruction VARCHAR (8),
    param_one VARCHAR (256),
    param_two VARCHAR (256),
    param_three VARCHAR (256),
    PRIMARY KEY (instruction_id)
);

DROP TABLE IF EXISTS `taylor_glazier_social_network`.`Notifications`;

CREATE TABLE `taylor_glazier_social_network`.`Notifications`(
    notification_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    type VARCHAR(8) NOT NULL,
    image_thumb_path VARCHAR (128) NOT NULL,
    link VARCHAR (128) NOT NULL,
    short_text VARCHAR (64) NOT NULL,
    is_new TINYINT (1) NOT NULL,
    PRIMARY KEY (notification_id)
);


CREATE TABLE `taylor_glazier_social_network`.`User_Notifications`(
    user_id INT UNSIGNED NOT NULL,
    notification_id INT UNSIGNED NOT NULL,
    FOREIGN KEY (user_id) REFERENCES Users(user_id) ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY (notification_id) REFERENCES Notifications(notification_id) ON DELETE RESTRICT ON UPDATE CASCADE,
    PRIMARY KEY (user_id, notification_id)
);


INSERT INTO `taylor_glazier_social_network`.`Users`
VALUES (1, 'Taylor Glazier', 'tg754', 'Taylor from taylorglazier.com', 'taylor@taylorglazier.com', 'http://taylorglazier.com/image-storage/user/tg754/profile/profile-tg754.jpg', 0, '5d41402abc4b2a76b9719d911017c592', '', '', 0, 1, 1, '2021-02-22');

INSERT INTO `taylor_glazier_social_network`.`Users`
VALUES (2, 'Hershey Bear', 'hershmurph', 'play', 'herschel@taylorglazier.com' 'http://taylorglazier.com/image-storage/user/hershmurph/profile/profile-hershmurph.jpg', 0, '5d41402abc4b2a76b9719d911017c592', '', '', 0, 1, 1, '2021-02-22');